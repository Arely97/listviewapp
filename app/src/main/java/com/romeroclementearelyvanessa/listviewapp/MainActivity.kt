package com.romeroclementearelyvanessa.listviewapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas : ArrayList<String> = ArrayList()
        frutas.add("Manzana")
        frutas.add("Platano")
        frutas.add("Guayaba")
        frutas.add("Kiwis")
        frutas.add("Limon")
        frutas.add("Uvas")
        frutas.add("Naranja")
        frutas.add("Fresa")
        frutas.add("Mango")
        frutas.add("Sandia")

        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,frutas)

        lista.adapter= adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(this,frutas.get(i), Toast.LENGTH_SHORT).show()
            if(i==0){
                val intent=Intent(this,Apple::class.java)
                startActivity(intent)
            }else  if (i==1){
                val intent=Intent(this,Banana::class.java)
                startActivity(intent)
            }
            else  if (i==2){
                val intent=Intent(this,Guava::class.java)
                startActivity(intent)
            }
            else  if (i==3){
                val intent=Intent(this,Kiwi::class.java)
                startActivity(intent)
            }
            else  if (i==4){
                val intent=Intent(this,Lemon::class.java)
                startActivity(intent)
            }
            else  if (i==5){
                val intent=Intent(this,Grapes::class.java)
                startActivity(intent)
            }else  if (i==6){
                val intent=Intent(this,Orange::class.java)
                startActivity(intent)
            }else  if (i==7){
                val intent=Intent(this,Strawberry::class.java)
                startActivity(intent)
            }
            else  if (i==8){
                val intent=Intent(this,Mango::class.java)
                startActivity(intent)
            }
            else{
                val intent=Intent(this,Watermelon::class.java)
                startActivity(intent)
            }
        }




    }
}